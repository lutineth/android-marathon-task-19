package com.example.hw19

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import kotlinx.coroutines.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MyService: Service() {

    private val tag = "MyService"

    init {
        Log.i(tag, "In progress")
    }

    override fun onBind(p0: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(ApiInterface::class.java)
        CoroutineScope(Dispatchers.IO).launch {
            Log.i(tag, "${retrofitBuilder.getData()}")
        }
        return START_REDELIVER_INTENT
    }
}