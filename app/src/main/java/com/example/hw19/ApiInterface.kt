package com.example.hw19

import retrofit2.http.GET

interface ApiInterface {
    @GET("posts")
    suspend fun getData(): List<MyDataItem>
}