package com.example.hw19

data class MyDataItem(
    val id: Int,
    val userId: Int,
    val title: String,
    val body: String
)