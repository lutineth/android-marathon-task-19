package com.example.hw19

import android.view.*
import androidx.recyclerview.widget.RecyclerView
import com.example.hw19.databinding.RowItemsBinding

class MyAdapter : RecyclerView.Adapter<MyAdapter.ViewHolder>(){
    val userList = ArrayList<MyDataItem>()

    class ViewHolder(item: View): RecyclerView.ViewHolder(item) {
        private val binding = RowItemsBinding.bind(item)

        fun bind(myDataItem: MyDataItem) = with(binding){
            uidText.text = myDataItem.userId.toString()
            idText.text = myDataItem.id.toString()
            titleText.text = myDataItem.title
            bodyText.text = myDataItem.body
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.row_items, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(userList[position])
    }

    fun add(item: MyDataItem){
        userList.add(item)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    fun clear(){
        userList.clear()
        notifyDataSetChanged()
    }
}