package com.example.hw19

import android.content.Intent
import androidx.work.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.hw19.databinding.ActivityMainBinding
import java.util.concurrent.TimeUnit

const val BASE_URL = "https://jsonplaceholder.typicode.com/"

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: MyAdapter
    companion object{
        var userList:List<MyDataItem> = listOf()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        adapter = MyAdapter()
        binding.rcView.layoutManager = LinearLayoutManager(applicationContext)
        binding.rcView.adapter = adapter

        periodicDataRequest()

        binding.StartService.setOnClickListener {
            Intent(this, MyService::class.java).also{
                startService(it)
            }
            binding.tvService.text = "In progress"
        }

        binding.btGetData.setOnClickListener {
            if(adapter.userList.isNotEmpty()){
                adapter.clear()
            }
            for(i in userList){
                adapter.add(i)
            }
        }
    }

    private fun periodicDataRequest(){
        val workManager = WorkManager.getInstance(applicationContext)
        val periodicWorkRequest = PeriodicWorkRequest.Builder(MyWorker::class.java, 10, TimeUnit.MINUTES).build()
        workManager.enqueue(periodicWorkRequest)
        if(adapter.userList.isNotEmpty()) {
            adapter.clear()
            for (i in userList){
                adapter.add(i)
            }
        } else{
            for (i in userList){
                adapter.add(i)
            }
        }

    }
}