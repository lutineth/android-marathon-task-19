package com.example.hw19

import android.content.Context
import android.util.Log
import androidx.work.*
import kotlinx.coroutines.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MyWorker(context: Context, parameters: WorkerParameters): Worker(context, parameters) {

    private val tag = "MyWorker"

    override fun doWork(): Result {
        return try {
            val retrofitBuilder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(ApiInterface::class.java)
            CoroutineScope(Dispatchers.IO).launch {
                MainActivity.userList =  retrofitBuilder.getData()
                Log.i(tag, "Updating")
            }
            Result.success()
        } catch (e: Exception){
            Result.failure()
        }
    }
}